#!/bin/sh
if [ "$1" ]
then
  file_path=$1
  file_name=$(basename "$file_path")
  if [ -f $file_path ]; then
    file_size=$(stat -c%s $file_path)
    md5=$(cat "$file_path.md5sum" | cut -d' ' -f1)
    datetime=$(grep ro\.build\.date\.utc $OUT/system/build.prop | cut -d= -f2);
    id=$(sha256sum $file_path | awk '{ print $1 }');
    echo "{\n   \"error\": \"false\",\n   \"filename\": \"$file_name\",\n   \"datetime\": $datetime,\n   \"size\": $file_size,\n   \"url\": \"https://sourceforge.net/projects/revos-aospq/files/$CUSTOM_BUILD/$file_name/download\",\n   \"filehash\": \"$md5\",\n   \"version\": \"Ten\",\n   \"id\": \"$id\",\n   \"website_url\": \"https://revtechs.me\",\n   \"news_url\": \"https://t.me/revolutionnews\",\n   \"donate_url\": \"https://www.paypal.me/badwolfalfa\",\n   \"maintainer\": \"BadWolf (LoboMalo_SoyDeGatita)\",\n   \"maintainer_url\": \"https://t.me/LoboMalo_SoyDeGatita\",\n   \"telegram_username\": \"LoboMalo_SoyDeGatita\",\n   \"forum_url\": \"https://revtechs.me/kunena/$CUSTOM_BUILD\"\n}" > $OUT/$CUSTOM_BUILD.json
  fi
fi

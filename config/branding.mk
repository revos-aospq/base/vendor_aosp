# Versioning System
REVOS_BASE_VERSION = 20.5.18
REVOS_CODENAME := Zombieland
REVOS_DONATE_URL ?= Unknown
REVOS_MAINTAINER ?= Unknown
REVOS_SUPPORT_URL ?= https://t.me/RevOSNews

# Set all versions
CUSTOM_BUILD_TYPE ?= UNOFFICIAL

# Set all versions
CUSTOM_BUILD_MAINTAINNER ?= BadWolf

CUSTOM_DATE_YEAR := $(shell date -u +%Y)
CUSTOM_DATE_MONTH := $(shell date -u +%m)
CUSTOM_DATE_DAY := $(shell date -u +%d)
CUSTOM_DATE_HOUR := $(shell date -u +%H)
CUSTOM_DATE_MINUTE := $(shell date -u +%M)
CUSTOM_BUILD_DATE_UTC := $(shell date -d '$(CUSTOM_DATE_YEAR)-$(CUSTOM_DATE_MONTH)-$(CUSTOM_DATE_DAY) $(CUSTOM_DATE_HOUR):$(CUSTOM_DATE_MINUTE) UTC' +%s)
CUSTOM_BUILD_DATE := $(CUSTOM_DATE_YEAR)$(CUSTOM_DATE_MONTH)$(CUSTOM_DATE_DAY)-$(CUSTOM_DATE_HOUR)$(CUSTOM_DATE_MINUTE)

CUSTOM_PLATFORM_VERSION := 10.0

TARGET_PRODUCT_SHORT := $(subst aosp_,,$(CUSTOM_BUILD))

REVOS_VERSION := $(REVOS_CODENAME)-v$(REVOS_BASE_VERSION)-$(TARGET_PRODUCT_SHORT)-$(CUSTOM_BUILD_DATE)-$(CUSTOM_BUILD_TYPE)

CUSTOM_VERSION := RevOS_$(REVOS_BASE_VERSION)_$(CUSTOM_BUILD)-$(CUSTOM_PLATFORM_VERSION)-$(CUSTOM_BUILD_DATE)-$(CUSTOM_BUILD_TYPE)
CUSTOM_VERSION_PROP := 10

CUSTOM_PROPERTIES := \
    org.revos.version=$(REVOS_VERSION) \
    org.revos.version.prop=$(CUSTOM_PLATFORM_VERSION) \
    org.revos.version.display=$(CUSTOM_VERSION) \
    org.revos.build_version=$(REVOS_BASE_VERSION) \
    org.revos.build_date=$(CUSTOM_BUILD_DATE) \
    org.revos.build_date_utc=$(CUSTOM_BUILD_DATE_UTC) \
    org.revos.build_type=$(CUSTOM_BUILD_TYPE) \
    org.revos.maintainner=$(CUSTOM_BUILD_MAINTAINNER) \
    org.revos.build_codename=$(REVOS_CODENAME)


# RomStats
PRODUCT_PACKAGES += \
    Contacts \
    Dialer \
    pixelbridge \
    messaging \
    RomStats \
    Lawnfeed

PRODUCT_PROPERTY_OVERRIDES += \
    ro.romstats.url=https://revtechs.me/api/stats/aospq/ \
    ro.romstats.name=RevOS-AOSPQ \
    ro.romstats.askfirst=1 \
    ro.romstats.version=$(CUSTOM_VERSION) \
    ro.romstats.tframe=6 \
    org.revos.build_donate_url=$(REVOS_DONATE_URL) \
    org.revos.build_maintainer=$(REVOS_MAINTAINER) \
    org.revos.build_support_url=$(REVOS_SUPPORT_URL)
